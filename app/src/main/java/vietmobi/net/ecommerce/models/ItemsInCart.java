package vietmobi.net.ecommerce.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "all_items_cart")
public class ItemsInCart  {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String imgItem;
    private String idItem, itemName, ofBrand, category, size, color;
    private Float price;
    int count;
    private boolean isCheckOut;

    public ItemsInCart() {
    }

    public ItemsInCart(String idItem, String imgItem,  String itemName, String ofBrand, String category, String size, String color,
                         Float price, int count, boolean isCheckOut) {
        this.imgItem = imgItem;
        this.idItem = idItem;
        this.itemName = itemName;
        this.ofBrand = ofBrand;
        this.category = category;
        this.size = size;
        this.color = color;
        this.price = price;
        this.count = count;
        this.isCheckOut = isCheckOut;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgItem() {
        return imgItem;
    }

    public void setImgItem(String imgItem) {
        this.imgItem = imgItem;
    }

    public String getIdItem() {
        return idItem;
    }

    public void setIdItem(String idItem) {
        this.idItem = idItem;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getOfBrand() {
        return ofBrand;
    }

    public void setOfBrand(String ofBrand) {
        this.ofBrand = ofBrand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isCheckOut() {
        return isCheckOut;
    }

    public void setCheckOut(boolean checkOut) {
        isCheckOut = checkOut;
    }
}
