package vietmobi.net.ecommerce.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "all_items_favorite")
public class ItemsFavorite {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String imgItems;
    private String idItem, itemName, ofBrand, category, size, color;
    private Float price;
    int count;
    private boolean isSale, isFavorite, isNew, isCheckOut;

    public ItemsFavorite() {
    }

    public ItemsFavorite(String idItem, String imgItems,  String itemName, String ofBrand, String category, String size, String color,
                         Float price, int count, boolean isSale, boolean isFavorite, boolean isNew, boolean isCheckOut) {
        this.imgItems = imgItems;
        this.idItem = idItem;
        this.itemName = itemName;
        this.ofBrand = ofBrand;
        this.category = category;
        this.size = size;
        this.color = color;
        this.price = price;
        this.count = count;
        this.isSale = isSale;
        this.isFavorite = isFavorite;
        this.isNew = isNew;
        this.isCheckOut = isCheckOut;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getImgItems() {
        return imgItems;
    }

    public void setImgItems(String imgItems) {
        this.imgItems = imgItems;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdItem() {
        return idItem;
    }

    public void setIdItem(String idItem) {
        this.idItem = idItem;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getOfBrand() {
        return ofBrand;
    }

    public void setOfBrand(String ofBrand) {
        this.ofBrand = ofBrand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }


    public boolean isSale() {
        return isSale;
    }

    public void setSale(boolean sale) {
        isSale = sale;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public boolean isCheckOut() {
        return isCheckOut;
    }

    public void setCheckOut(boolean checkOut) {
        isCheckOut = checkOut;
    }


}
