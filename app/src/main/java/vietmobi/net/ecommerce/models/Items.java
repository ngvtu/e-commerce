package vietmobi.net.ecommerce.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Items implements Serializable {
    public String getImgItems() {
        return imgItems;
    }

    public void setImgItems(String imgItems) {
        this.imgItems = imgItems;
    }

    private String  imgItems;
    private String id, itemName, ofBrand, category, describe;
    private Float price, saleValue, newPrice;
    private boolean isSale, isFavorite, isNew;

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Map<String, Object > toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("itemName", itemName);
        result.put("ofBrand", ofBrand);
        result.put("category", category);
        result.put("describe", describe);
        result.put("price", price);
        result.put("saleValue", saleValue);
        result.put("newPrice", newPrice);
        result.put("isSale", isSale);
        result.put("isFavorite", isFavorite);
        return result;
    }

    public Items(String id, String itemName, String ofBrand, String category, String describe, Float price, Float saleValue, boolean isSale, boolean isNew) {
        this.id = id;
        this.itemName = itemName;
        this.ofBrand = ofBrand;
        this.category = category;
        this.describe = describe;
        this.price = price;
        this.saleValue = saleValue;
        this.isSale = isSale;
        this.isNew = isNew;
    }

    public Items(String id, String imgItems, String itemName, String ofBrand, String category, Float price, boolean isSale, boolean isFavorite) {
        this.id = id;
        this.imgItems = imgItems;
        this.itemName = itemName;
        this.ofBrand = ofBrand;
        this.category = category;
        this.price = price;
        this.isSale = isSale;
        this.isFavorite = isFavorite;
    }

    public Items(String imgItems, String itemName, String ofBrand, String category, Float price, boolean isSale, boolean isFavorite) {
        this.imgItems = imgItems;
        this.itemName = itemName;
        this.ofBrand = ofBrand;
        this.category = category;
        this.price = price;
        this.isSale = isSale;
        this.isFavorite = isFavorite;
    }

    public Items(String imgItems, String itemName, String ofBrand, String category, Float price) {
        this.imgItems = imgItems;
        this.itemName = itemName;
        this.ofBrand = ofBrand;
        this.category = category;
        this.price = price;
    }

    public Items(String imgItems, String itemName, String ofBrand, Float price, Float saleValue, Float newPrice) {
        this.imgItems = imgItems;
        this.itemName = itemName;
        this.ofBrand = ofBrand;
        this.price = price;
        this.saleValue = saleValue;
        this.newPrice = newPrice;
    }

    public Items() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getOfBrand() {
        return ofBrand;
    }

    public void setOfBrand(String ofBrand) {
        this.ofBrand = ofBrand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getSaleValue() {
        return saleValue;
    }

    public void setSaleValue(Float saleValue) {
        this.saleValue = saleValue;
    }

    public Float getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(Float newPrice) {
        this.newPrice = newPrice;
    }

    public boolean isSale() {
        return isSale;
    }

    public void setSale(boolean sale) {
        isSale = sale;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }


}
