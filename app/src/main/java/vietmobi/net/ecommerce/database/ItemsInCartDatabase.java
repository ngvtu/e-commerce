package vietmobi.net.ecommerce.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import vietmobi.net.ecommerce.models.ItemsInCart;

@Database(entities = {ItemsInCart.class}, version = 1)
public abstract class ItemsInCartDatabase extends RoomDatabase{
    public static final String DATABASE_NAME = "items_in_cart.db";
    private static ItemsInCartDatabase instance;

    public static synchronized ItemsInCartDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), ItemsInCartDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public abstract ItemsInCartDAO itemsInCartDAO();
}
