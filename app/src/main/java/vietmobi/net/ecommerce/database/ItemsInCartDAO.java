package vietmobi.net.ecommerce.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import vietmobi.net.ecommerce.models.ItemsInCart;

@Dao
public interface ItemsInCartDAO {
    @Insert
    void insertItemInCart(ItemsInCart itemsInCart);

    @Query("select * from all_items_cart where id = :id")
    ItemsInCart getItemInCart(int id);

    @Query("select count(id) from all_items_cart")
    int getCountItems();

    @Query("select * from all_items_cart")
    List<ItemsInCart> getListItemInCart();

    @Query("SELECT SUM(price) FROM all_items_cart")
    float getTotalAmount();


    @Update
    void updateItemsInCart(ItemsInCart itemsInCart);

    @Delete
    void deleteItemsInCart(ItemsInCart itemsInCart);

}
