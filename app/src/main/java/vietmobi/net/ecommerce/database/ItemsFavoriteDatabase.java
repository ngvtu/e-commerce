package vietmobi.net.ecommerce.database;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import vietmobi.net.ecommerce.models.ItemsFavorite;

@Database(entities = {ItemsFavorite.class}, version = 1)
public abstract class ItemsFavoriteDatabase extends RoomDatabase {
    public static final String DATABASE_NAME = "items_favorite.db";
    private static ItemsFavoriteDatabase instance;

    public static synchronized ItemsFavoriteDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), ItemsFavoriteDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public abstract ItemsFavoriteDAO itemsFavoriteDAO();

}
