package vietmobi.net.ecommerce.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import vietmobi.net.ecommerce.models.ItemsFavorite;

@Dao
public interface ItemsFavoriteDAO {
    @Insert
    void insertItemsFavorite(ItemsFavorite itemsFavorite);

    @Query("select * from all_items_favorite where id = :id")
    ItemsFavorite getItemsFavorite(int id);

    @Query("select count(id) from all_items_favorite")
    int getCountItems();

    @Query("select * from all_items_favorite")
    List<ItemsFavorite> getListItemsFavorite();

    @Update
    void updateItemsFavorite(ItemsFavorite itemsFavorite);

    @Delete
    void deleteItemsFavorite(ItemsFavorite itemsFavorite);
}
