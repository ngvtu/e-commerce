package vietmobi.net.ecommerce.activity.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.adapter.ItemsFavoriteAdapter;
import vietmobi.net.ecommerce.adapter.ItemsInCartAdapter;
import vietmobi.net.ecommerce.database.ItemsFavoriteDatabase;
import vietmobi.net.ecommerce.database.ItemsInCartDatabase;
import vietmobi.net.ecommerce.models.Items;
import vietmobi.net.ecommerce.models.ItemsFavorite;
import vietmobi.net.ecommerce.models.ItemsInCart;

public class DetailItemActivity extends AppCompatActivity {

    TextView tvNameItem, tvNameBrand, tvCategory, tvDescription, btnAddToCart, tvPrice;
    AppCompatAutoCompleteTextView edtSize, edtColor;
    TextInputLayout layout_size, layout_color;
    ImageView btnBack, imgItem, btnShare;
    ImageButton btnAddFavorite;
    Items item;

    ItemsInCartAdapter itemsInCartAdapter;
    String[] size = {"XS", "S", "M", "L", "XL"};
    String[] color = {"Back", "Gray", "White"};
    private ItemsInCart itemsInCart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_item);

        initViews();
        addEvents();
    }

    private void addEvents() {

        getItemShowDetail();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareItem();
            }
        });

        btnAddFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToFavorite();
            }
        });

        ArrayAdapter<String> adapterSize;
        adapterSize = new ArrayAdapter<String>(DetailItemActivity.this, R.layout.line_list_type_items, size);
        edtSize.setAdapter(adapterSize);
        edtSize.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String size = parent.getItemAtPosition(position).toString();
                edtSize.setText(size);
            }
        });

        ArrayAdapter<String> adapterColor;
        adapterColor = new ArrayAdapter<String>(DetailItemActivity.this, R.layout.line_list_type_items, color);
        edtColor.setAdapter(adapterColor);
        edtColor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String color = parent.getItemAtPosition(position).toString();
                edtColor.setText(color);
            }
        });

        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCart();
            }
        });


    }

    private void addToCart() {
        if (!edtColor.getText().toString().equals("") && !edtSize.getText().toString().equals("")) {
            layout_size.setError("");
            layout_color.setError("");
            String size = edtSize.getText().toString();
            String color = edtColor.getText().toString();
            float price = 0;
            if (item.isSale()) {
                price = (float) (Math.round((item.getPrice() / 100) * (100 - item.getSaleValue()) * 100.0) / 100.0);
            } else if (item.isNew()) {
                price = item.getPrice();
            }
            ItemsInCart itemsInCart = new ItemsInCart(item.getId(), item.getImgItems(), item.getItemName(), item.getOfBrand(),
                    item.getCategory(), size, color, price, 1, false);

            ItemsInCartDatabase.getInstance(DetailItemActivity.this).itemsInCartDAO().insertItemInCart(itemsInCart);

            Toast.makeText(DetailItemActivity.this, "Add to cart", Toast.LENGTH_SHORT).show();
            List<ItemsInCart> listItemsInCart;
            listItemsInCart = ItemsInCartDatabase.getInstance(DetailItemActivity.this).itemsInCartDAO().getListItemInCart();

            if (listItemsInCart == null) {
                return;
            }
//            BagFragment bagFragment = new BagFragment();
//
//            bagFragment.updateData();

            ItemsInCartAdapter itemsInCartAdapter = new ItemsInCartAdapter(listItemsInCart, DetailItemActivity.this);
            itemsInCartAdapter.setData(listItemsInCart);
            itemsInCartAdapter.notifyDataSetChanged();
            setResult(RESULT_OK);
            finish();
        } else if (edtSize.getText().toString().equals("")) {
            edtSize.requestFocus();
            layout_size.setError("*Choose size");
            layout_color.setError("");
        } else if (edtColor.getText().toString().equals("")) {
            edtColor.requestFocus();
            layout_color.setError("*Choose color");
            layout_size.setError("");
        }
    }

    private void getItemShowDetail() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return;
        }
        item = (Items) bundle.get("item");
        tvNameItem.setText(item.getItemName());
        tvNameBrand.setText(item.getOfBrand());
        tvDescription.setText(item.getDescribe());
        tvCategory.setText(item.getCategory());
        if (item.isSale()) {
            float newPrice = (item.getPrice() / 100) * (100 - item.getSaleValue());
            tvPrice.setText(Float.toString(newPrice));
            tvPrice.setTextColor(Color.parseColor("#DB3022"));
        } else if (item.isNew()) {
            tvPrice.setText(item.getPrice().toString());
        }
        Glide.with(DetailItemActivity.this).load(item.getImgItems()).into(imgItem);
    }

    private void addToFavorite() {
        ItemsFavoriteAdapter itemsFavoriteAdapter = new ItemsFavoriteAdapter();

        if (!edtColor.getText().toString().equals("") && !edtSize.getText().toString().equals("")) {
            layout_size.setError("");
            layout_color.setError("");
            String size = edtSize.getText().toString();
            String color = edtColor.getText().toString();
            float price = 0;
            if (item.isSale()) {
//                price = ;
                price = (float) (Math.round((item.getPrice() / 100) * (100 - item.getSaleValue()) * 100.0) / 100.0);
            } else if (item.isNew()) {
                price = item.getPrice();
            }
            ItemsFavorite itemsFavorite = new ItemsFavorite(item.getId(), item.getImgItems(), item.getItemName(), item.getOfBrand(),
                    item.getCategory(), size, color, price, 1, item.isSale(), true, item.isNew(), false);

            ItemsFavoriteDatabase.getInstance(DetailItemActivity.this).itemsFavoriteDAO().insertItemsFavorite(itemsFavorite);

            Toast.makeText(DetailItemActivity.this, "Add to favorite", Toast.LENGTH_SHORT).show();
            List<ItemsFavorite> listItemsFavorites;
            listItemsFavorites = ItemsFavoriteDatabase.getInstance(DetailItemActivity.this).itemsFavoriteDAO().getListItemsFavorite();

            if (listItemsFavorites == null) {
                return;
            }
//            FavoriteFragment favoriteFragment = new FavoriteFragment();
//
//            favoriteFragment.updateData();
            itemsFavoriteAdapter.setData(listItemsFavorites);
            itemsFavoriteAdapter.notifyDataSetChanged();
            setResult(RESULT_OK);
            finish();

        } else if (edtSize.getText().toString().equals("")) {
            edtSize.requestFocus();
            layout_size.setError("*Choose size");
            layout_color.setError("");
        } else if (edtColor.getText().toString().equals("")) {
            edtColor.requestFocus();
            layout_color.setError("*Choose color");
            layout_size.setError("");
        }

    }

    private void shareItem() {
        Toast.makeText(DetailItemActivity.this, "Share item coming soon", Toast.LENGTH_SHORT).show();
    }

    private void initViews() {
        tvNameItem = findViewById(R.id.tvNameItem);
        tvNameBrand = findViewById(R.id.tvNameBrand);
        tvCategory = findViewById(R.id.tvCategory);
        tvDescription = findViewById(R.id.tvDescription);
        btnAddToCart = findViewById(R.id.btnAddToCart);
        tvPrice = findViewById(R.id.tvPrice);
        edtSize = findViewById(R.id.edtSize);
        edtColor = findViewById(R.id.edtColor);
        layout_size = findViewById(R.id.layout_size);
        layout_color = findViewById(R.id.layout_color);
        btnBack = findViewById(R.id.btnBack);
        imgItem = findViewById(R.id.imgItem);
        btnAddFavorite = findViewById(R.id.btnAddFavorite);
        btnShare = findViewById(R.id.btnShare);
    }
}