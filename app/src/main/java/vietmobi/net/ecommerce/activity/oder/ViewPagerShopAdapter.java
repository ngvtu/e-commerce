package vietmobi.net.ecommerce.activity.oder;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class ViewPagerShopAdapter extends FragmentStatePagerAdapter {

    public ViewPagerShopAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new NewAndSaleFragment();
            case 1:
                return new ClothesFragment();
            case 2:
                return new AccessoryFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
