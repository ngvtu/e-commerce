package vietmobi.net.ecommerce.activity.mainadmmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.activity.main.AddItemsActivity;
import vietmobi.net.ecommerce.activity.main.MainActivity;
import vietmobi.net.ecommerce.activity.main.ViewPagerMainAdapter;

public class MainAdminActivity extends AppCompatActivity {

    private static final int MY_REQUEST_CODE_ADD = 1001;
    BottomNavigationView bottomNavigation;
    FloatingActionButton btnAddItem;
    ViewPager viewPager;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_admin);

        initViews();
        addEvents();
    }

    private void addEvents() {
        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainAdminActivity.this, AddItemsActivity.class);
                startActivityForResult(intent, MY_REQUEST_CODE_ADD);
            }
        });

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item:
                        viewPager.setCurrentItem(0);
                        break;
                    case R.id.oder:
                        viewPager.setCurrentItem(1);
                        break;
                    case R.id.statistical:
                        viewPager.setCurrentItem(2);
                        break;
                    case R.id.user:
                        viewPager.setCurrentItem(3);
                        break;
                }
                return true;
            }
        });

        ViewPagerMainAdminAdapter viewPagerMainAdminAdapter = new ViewPagerMainAdminAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(viewPagerMainAdminAdapter);
        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        bottomNavigation.getMenu().findItem(R.id.item).setChecked(true);
                        break;
                    case 1:
                        bottomNavigation.getMenu().findItem(R.id.oder).setChecked(true);
                        break;
                    case 2:
                        bottomNavigation.getMenu().findItem(R.id.statistical).setChecked(true);
                        break;
                    case 3:
                        bottomNavigation.getMenu().findItem(R.id.user).setChecked(true);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void initViews() {
        bottomNavigation = findViewById(R.id.bottomNavigation);
        btnAddItem = findViewById(R.id.btnAddItem);
        viewPager = findViewById(R.id.viewPager);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


}