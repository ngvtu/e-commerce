package vietmobi.net.ecommerce.activity.main;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.adapter.ItemsAdapter;
import vietmobi.net.ecommerce.models.Items;


public class HomeFragment extends Fragment implements View.OnClickListener {

    ImageView imvBgHome;
    TextView btnViewAllItemNew, btnViewAllItemSale;
    RecyclerView rcvListItemSale, rcvListItemNew;


    private List<Items> listItemsSale;
    private List<Items> listItemsNew;

    ItemsAdapter itemsAdapter;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("list_items");
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        initViews(view);
        addItemsNew();
        addItemsSale();

        addEvents();
        return view;
    }



    private void addItemsSale() {
        listItemsSale = new ArrayList<>();

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Items items = snapshot.getValue(Items.class);
                if (listItemsSale != null && items.isSale()){
                    listItemsSale.add(items);
                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                itemsAdapter = new ItemsAdapter(listItemsSale, getActivity());
                itemsAdapter.notifyDataSetChanged();
                rcvListItemSale.setAdapter(itemsAdapter);
                rcvListItemSale.setLayoutManager(linearLayoutManager);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Items items = snapshot.getValue(Items.class);
//                if (listItemsSale != null){
//                    listItemsSale.add(items);
//                }
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                itemsAdapter = new ItemsAdapter(listItemsSale, getActivity());
                itemsAdapter.notifyDataSetChanged();
                rcvListItemSale.setAdapter(itemsAdapter);
                rcvListItemSale.setLayoutManager(linearLayoutManager);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void addItemsNew() {
        listItemsNew = new ArrayList<>();

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Items items = snapshot.getValue(Items.class);
                if (listItemsNew != null && items.isNew()){
                    listItemsNew.add(items);
                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                itemsAdapter = new ItemsAdapter(listItemsNew, getActivity());
                itemsAdapter.notifyDataSetChanged();
                rcvListItemNew.setAdapter(itemsAdapter);
                rcvListItemNew.setLayoutManager(linearLayoutManager);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Items items = snapshot.getValue(Items.class);
//                if (listItemsSale != null){
//                    listItemsSale.add(items);
//                }
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                itemsAdapter = new ItemsAdapter(listItemsSale, getActivity());
                itemsAdapter.notifyDataSetChanged();
                rcvListItemSale.setAdapter(itemsAdapter);
                rcvListItemSale.setLayoutManager(linearLayoutManager);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        itemsAdapter = new ItemsAdapter(listItemsNew, getActivity());
        rcvListItemNew.setAdapter(itemsAdapter);
        rcvListItemNew.setLayoutManager(linearLayoutManager);
    }

    private void addEvents() {
        btnViewAllItemNew.setOnClickListener(this);
        btnViewAllItemSale.setOnClickListener(this);

    }

    private void initViews(View view) {
        imvBgHome = view.findViewById(R.id.imvBgHome);
        btnViewAllItemNew = view.findViewById(R.id.btnViewAllItemNew);
        btnViewAllItemSale = view.findViewById(R.id.btnViewAllItemSale);
        rcvListItemSale = view.findViewById(R.id.rcvListItemSale);
        rcvListItemNew = view.findViewById(R.id.rcvListItemNew);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnViewAllItemNew:
                Intent intent = new Intent(getContext(), ViewAllItemsNewActivity.class);
                startActivity(intent);
                break;
            case R.id.btnViewAllItemSale:
                Intent intent2 = new Intent(getContext(), ViewAllItemsSaleActivity.class);
                startActivity(intent2);
                break;
        }
    }
}