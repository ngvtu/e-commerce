package vietmobi.net.ecommerce.activity.mainadmmin;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


public class ViewPagerMainAdminAdapter extends FragmentStatePagerAdapter {
    public ViewPagerMainAdminAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ItemsManagerFragment();
            case 1:
                return new OderManagerFragment();
            case 2:
                return new StatisticalManagerFragment();
            case 3:
                return new UserManagerFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
