package vietmobi.net.ecommerce.activity.oder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import vietmobi.net.ecommerce.R;

public class MyOrdersActivity extends AppCompatActivity {
    ImageView btnBack;
    ColorStateList def;
    ViewPager viewPager;
    TextView item1, item2, item3, select;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);

        initViews();
        addEvents();
    }

    private void addEvents() {
    }

    private void initViews() {
    }

}