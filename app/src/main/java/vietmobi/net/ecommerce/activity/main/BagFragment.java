package vietmobi.net.ecommerce.activity.main;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

import static vietmobi.net.ecommerce.adapter.AddressesAdapter.MY_REQUEST_CODE;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import vietmobi.net.ecommerce.OnApplyButtonClickListener;
import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.activity.CheckoutActivity;
import vietmobi.net.ecommerce.activity.PromoCodesActivity;
import vietmobi.net.ecommerce.adapter.ItemsInCartAdapter;
import vietmobi.net.ecommerce.adapter.PromoCodeAdapter;
import vietmobi.net.ecommerce.database.AddressDatabase;
import vietmobi.net.ecommerce.database.ItemsInCartDatabase;
import vietmobi.net.ecommerce.models.ItemsInCart;

public class BagFragment extends Fragment implements View.OnClickListener, OnApplyButtonClickListener {

    TextView tvNotification, tvPrice, btnCheckOut, tvPromoCode;
    RecyclerView rcvListItemInBag;
    ImageView imageView, btnGotoPromo, btnX, btnReload;
    RelativeLayout layout_bag;


    List<ItemsInCart> listItemsInCart;
    ItemsInCartAdapter itemsInCartAdapter;
    SharedPreferences sharedpreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bag, container, false);

        initViews(view);
        addEvents(view);
        return view;

    }

    public BagFragment() {
    }

    private void addEvents(View view) {
        btnCheckOut.setOnClickListener(this);
        btnGotoPromo.setOnClickListener(this);
        btnX.setOnClickListener(this);
        btnReload.setOnClickListener(this);


        listItemsInCart = new ArrayList<>();
        listItemsInCart = ItemsInCartDatabase.getInstance(getContext()).itemsInCartDAO().getListItemInCart();

        if (listItemsInCart.isEmpty()) {
            tvNotification.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
            layout_bag.setVisibility(View.GONE);
            return;
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        itemsInCartAdapter = new ItemsInCartAdapter(listItemsInCart, getContext(), this);
        itemsInCartAdapter.notifyDataSetChanged();
        rcvListItemInBag.setAdapter(itemsInCartAdapter);
        rcvListItemInBag.setLayoutManager(linearLayoutManager);

        float price = ItemsInCartDatabase.getInstance(getContext()).itemsInCartDAO().getTotalAmount();
        tvPrice.setText(String.valueOf(price));

//        SharedPreferences sharedPref = getContext().getSharedPreferences("promoCode", MODE_PRIVATE);
//
//        String code = sharedPref.getString("code", "");
//        int valueCode = sharedPref.getInt("valuePromo", 0);
        tvPromoCode.setText("");
    }

    private void initViews(View view) {
        tvNotification = view.findViewById(R.id.tvNotification);
        rcvListItemInBag = view.findViewById(R.id.rcvListItemInBag);
        imageView = view.findViewById(R.id.imageView);
        tvPrice = view.findViewById(R.id.tvPrice);
        btnCheckOut = view.findViewById(R.id.btnCheckOut);
        tvPromoCode = view.findViewById(R.id.tvPromoCode);
        btnGotoPromo = view.findViewById(R.id.btnGotoPromo);
        layout_bag = view.findViewById(R.id.layout_bag);
        btnX = view.findViewById(R.id.btnX);
        btnReload = view.findViewById(R.id.btnReload);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCheckOut:
                checkOut();
                break;
            case R.id.btnGotoPromo:
                gotoPromo();
                break;
            case R.id.btnX:
                if (!tvPromoCode.getText().equals("")) {
                    tvPromoCode.setText("");
                    btnX.setVisibility(View.GONE);
                    SharedPreferences sharedPreferences = getContext().getSharedPreferences("promoCode", MODE_PRIVATE);
                    SharedPreferences.Editor myEdit = sharedPreferences.edit();
                    myEdit.putString("code", "");
                    myEdit.putInt("valuePromo", 0);
                    myEdit.apply();
                    updateData();
                    break;
                }
            case R.id.btnReload:
                onResume();
                updateData();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Update list and adapter
        listItemsInCart = ItemsInCartDatabase.getInstance(getContext()).itemsInCartDAO().getListItemInCart();
        sharedpreferences = getContext().getSharedPreferences("promoCode", MODE_PRIVATE);
        String code = sharedpreferences.getString("code", "");
        int valueCode = sharedpreferences.getInt("valuePromo", 0);

        if (listItemsInCart.isEmpty()) {
            tvNotification.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
            layout_bag.setVisibility(View.GONE);
            return;
        } else {
            ItemsInCartAdapter itemsInCartAdapter = new ItemsInCartAdapter(listItemsInCart, getContext(), this);
            itemsInCartAdapter.setData(listItemsInCart);
            itemsInCartAdapter.notifyDataSetChanged();
            tvNotification.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
            layout_bag.setVisibility(View.VISIBLE);
            float price = ItemsInCartDatabase.getInstance(getContext()).itemsInCartDAO().getTotalAmount();

            float priceSale = (price / 100) * (100 - valueCode);

            float countPrice = (float) (Math.round(priceSale * 100.0) / 100.0);

            tvPrice.setText(String.valueOf(countPrice));
            tvPromoCode.setText(code);
//            btnX.setVisibility(View.VISIBLE);
            updateData();
        }
    }

    public void updateData() {
        // Fetch updated data from database and update the RecyclerView
        listItemsInCart = ItemsInCartDatabase.getInstance(getContext()).itemsInCartDAO().getListItemInCart();

        sharedpreferences = getContext().getSharedPreferences("promoCode", MODE_PRIVATE);
        String code = sharedpreferences.getString("code", "");
        int valueCode = sharedpreferences.getInt("valuePromo", 0);

        if (listItemsInCart.isEmpty()) {
            tvNotification.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
            layout_bag.setVisibility(View.GONE);
            return;
        }
        ItemsInCartAdapter itemsInCartAdapter = new ItemsInCartAdapter(listItemsInCart, getContext(), this);
        itemsInCartAdapter.setData(listItemsInCart);
        itemsInCartAdapter.notifyDataSetChanged();
        // Update the total price text view
        float price = ItemsInCartDatabase.getInstance(getContext()).itemsInCartDAO().getTotalAmount();
        if (tvPrice != null) {
            float priceSale = (price / 100) * (100 - valueCode);
            float countPrice = (float) (Math.round(priceSale * 100.0) / 100.0);
            tvPrice.setText(String.valueOf(countPrice));
            tvPromoCode.setText(code);
            btnX.setVisibility(View.VISIBLE);
        }
    }

    private void gotoPromo() {
        Intent intent = new Intent(getContext(), PromoCodesActivity.class);
        startActivityForResult(intent, 999);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_OK) {
            listItemsInCart = ItemsInCartDatabase.getInstance(getContext()).itemsInCartDAO().getListItemInCart();
            itemsInCartAdapter.setData(listItemsInCart);

        }
    }

    private void checkOut() {
        Intent intent = new Intent(getContext(), CheckoutActivity.class);
        startActivityForResult(intent, 9909);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Oder", MODE_PRIVATE);

        SharedPreferences.Editor myEdit = sharedPreferences.edit();
        myEdit.putFloat("total_amount", Float.parseFloat(tvPrice.getText().toString()));
        myEdit.apply();
    }

    @Override
    public void onApplyButtonClick() {

    }
}