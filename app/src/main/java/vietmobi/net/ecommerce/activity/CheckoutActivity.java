package vietmobi.net.ecommerce.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import vietmobi.net.ecommerce.R;

public class CheckoutActivity extends AppCompatActivity {

    ImageView btnBack, imgDhl, imgUsps, imgFedex, imgMasterCard, imgVisaCard;
    RadioButton radioCash, radioCard;
    LinearLayout layout_payment;
    RadioGroup radioGroup;
    TextView tvPriceOder, tvPriceDelivery, tvPriceSummary, btnSubmitOder, tvNameUser, tvCity, btnChange,
            tvPlace, tvPhoneNumber, tvDistrict, tvCountry, tvEdit, tvCardName, tvExpiryDate, tv4Number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        initViews();
        addEvents();
    }

    private void addEvents() {
        setAddress();
        setPayment();
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        SharedPreferences sharedPref = getSharedPreferences("Oder", MODE_PRIVATE);
        float totalOder = sharedPref.getFloat("total_amount", 0);
        float summary = totalOder + 30;

        tvPriceOder.setText(String.valueOf(totalOder));
        tvPriceSummary.setText(String.valueOf(summary));

        btnSubmitOder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckoutActivity.this, SuccessActivity.class);
                startActivity(intent);
                finish();
            }
        });
        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckoutActivity.this, AddressesActivity.class);
                startActivity(intent);
            }
        });

        radioCard.setChecked(true);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioCard:
                        radioCard.setChecked(true);
                        layout_payment.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radioCash:
                        layout_payment.setVisibility(View.GONE);
                        break;
                }
            }
        });

        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckoutActivity.this, PaymentActivity.class);
                startActivity(intent);
            }
        });

        Drawable drawable = getResources().getDrawable(R.drawable.border_item);
        imgDhl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgDhl.setBackgroundDrawable(drawable);
                imgFedex.setBackgroundDrawable(null);
                imgUsps.setBackgroundDrawable(null);
                SharedPreferences sharedPreferences = getSharedPreferences("Oder", MODE_PRIVATE);

                SharedPreferences.Editor myEdit = sharedPreferences.edit();
                myEdit.putString("shipping_unit", "Shipping unit: Dhl");
                myEdit.apply();
            }
        });
        imgFedex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgDhl.setBackgroundDrawable(null);
                imgFedex.setBackgroundDrawable(drawable);
                imgUsps.setBackgroundDrawable(null);
                SharedPreferences sharedPreferences = getSharedPreferences("Oder", MODE_PRIVATE);

                SharedPreferences.Editor myEdit = sharedPreferences.edit();
                myEdit.putString("shipping_unit", "Shipping unit: Fedex");
                myEdit.apply();
            }
        });
        imgUsps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgDhl.setBackgroundDrawable(null);
                imgFedex.setBackgroundDrawable(null);
                imgUsps.setBackgroundDrawable(drawable);
                SharedPreferences sharedPreferences = getSharedPreferences("Oder", MODE_PRIVATE);

                SharedPreferences.Editor myEdit = sharedPreferences.edit();
                myEdit.putString("shipping_unit", "Shipping unit: Usps");
                myEdit.apply();
            }
        });
    }

    private void setAddress() {
        SharedPreferences sharedPref = getSharedPreferences("address_ship", MODE_PRIVATE);
        tvNameUser.setText(sharedPref.getString("full_name", ""));
        tvPlace.setText(sharedPref.getString("address", ""));
        tvDistrict.setText(sharedPref.getString("district", ""));
        tvCity.setText(sharedPref.getString("province", ""));
        tvCountry.setText(sharedPref.getString("country", ""));
        tvPhoneNumber.setText(sharedPref.getString("numberPhone", ""));
    }

    private void setPayment() {
        SharedPreferences sharedPref = getSharedPreferences("payment_card", MODE_PRIVATE);
        tvCardName.setText(sharedPref.getString("cardName", ""));
        tvExpiryDate.setText(sharedPref.getString("expiryDate", ""));
        tv4Number.setText(sharedPref.getString("numberCard", ""));
        String typeCard = sharedPref.getString("typeCard", "");
        if (typeCard.equals("VISA")){
            imgMasterCard.setVisibility(View.GONE);
            imgVisaCard.setVisibility(View.VISIBLE);
        } else if (typeCard.equals("Master Card")){
            imgMasterCard.setVisibility(View.VISIBLE);
            imgVisaCard.setVisibility(View.GONE);
        }
    }


    private void initViews() {
        btnBack = findViewById(R.id.btnBack);
        imgDhl = findViewById(R.id.imgDhl);
        imgUsps = findViewById(R.id.imgUsps);
        tvCity = findViewById(R.id.tvCity);
        imgFedex = findViewById(R.id.imgFedex);
        imgMasterCard = findViewById(R.id.imgMasterCard);
        imgVisaCard = findViewById(R.id.imgVisaCard);
        radioCash = findViewById(R.id.radioCash);
        radioGroup = findViewById(R.id.radioGroup);
        radioCard = findViewById(R.id.radioCard);
        btnChange = findViewById(R.id.btnChange);
        tvPriceOder = findViewById(R.id.tvPriceOder);
        tvPriceDelivery = findViewById(R.id.tvPriceDelivery);
        tvPriceSummary = findViewById(R.id.tvPriceSummary);
        btnSubmitOder = findViewById(R.id.btnSubmitOder);
        tvNameUser = findViewById(R.id.tvNameUser);
        tvPlace = findViewById(R.id.tvPlace);
        tvPhoneNumber = findViewById(R.id.tvPhoneNumber);
        tvDistrict = findViewById(R.id.tvDistrict);
        layout_payment = findViewById(R.id.layout_payment);
        tvCountry = findViewById(R.id.tvCountry);
        tvEdit = findViewById(R.id.tvEdit);
        tvCardName = findViewById(R.id.tvCardName);
        tvExpiryDate = findViewById(R.id.tvExpiryDate);
        tv4Number = findViewById(R.id.tv4Number);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAddress();
        setPayment();
    }


}