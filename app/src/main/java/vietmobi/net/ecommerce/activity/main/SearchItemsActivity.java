package vietmobi.net.ecommerce.activity.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.adapter.ItemsAdapter;
import vietmobi.net.ecommerce.models.Items;

public class SearchItemsActivity extends AppCompatActivity {

    ImageView btnBack;
    RecyclerView rcvSearch;
    SearchView searchView;

    List<Items> listItems;
    List<Items> searchResults;
    ItemsAdapter itemsAdapter;
    ArrayAdapter<Items> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_items);

        initViews();
        addEvents();

    }

    private void addEvents() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        listItems = new ArrayList<>();
        searchResults = new ArrayList<>();

        itemsAdapter = new ItemsAdapter(searchResults, SearchItemsActivity.this);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(SearchItemsActivity.this, 2);
        rcvSearch.setLayoutManager(linearLayoutManager);
        rcvSearch.setAdapter(itemsAdapter);


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("list_items");

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Items items = snapshot.getValue(Items.class);
                if (listItems != null) {
                    listItems.add(items);
                }

                itemsAdapter = new ItemsAdapter(listItems, SearchItemsActivity.this);
                LinearLayoutManager linearLayoutManager = new GridLayoutManager(SearchItemsActivity.this, 2);
                itemsAdapter.notifyDataSetChanged();
                rcvSearch.setLayoutManager(linearLayoutManager);
                rcvSearch.setAdapter(itemsAdapter);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                List<Items> searchResults = new ArrayList<>();
                for (Items item : listItems) {
                    if (item.getItemName().toLowerCase().contains(query.toLowerCase())) {
                        searchResults.add(item);
                    }
                }

                return false;

            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchResults.clear();
                for (Items item : listItems) {
                    if (item.getItemName().toLowerCase().contains(newText.toLowerCase())) {
                        searchResults.add(item);
                    }
                }
                itemsAdapter.notifyDataSetChanged();

                if (searchResults.isEmpty()) {
                    Toast.makeText(SearchItemsActivity.this, "BUGGGGGGGGGGGGGG", Toast.LENGTH_SHORT).show();
                } else {
                    itemsAdapter = new ItemsAdapter(searchResults, SearchItemsActivity.this);


                    rcvSearch.setLayoutManager(new GridLayoutManager(SearchItemsActivity.this, 2));
                    rcvSearch.setAdapter(itemsAdapter);

                }

                return false;
            }
        });
    }


    private void initViews() {
        btnBack = findViewById(R.id.btnBack);
        rcvSearch = findViewById(R.id.rcvSearch);
        searchView = findViewById(R.id.searchView);
    }
}