package vietmobi.net.ecommerce.activity.oder;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.activity.Dialog;
import vietmobi.net.ecommerce.adapter.ItemsAdapter;
import vietmobi.net.ecommerce.models.Items;


public class ClothesFragment extends Fragment {
    ImageView btnSort, btnViewItems;
    TextView tvSort;
    RecyclerView rcvListItems;

    private List<Items> listItemsClothes;

    ItemsAdapter itemsAdapter;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("list_items");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_clothes, container, false);
        initViews(view);
        addEvents(view);
        return view;
    }

    private void addEvents(View view) {
        listItemsClothes = new ArrayList<>();

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Items items = snapshot.getValue(Items.class);
                List<String> values = Arrays.asList("Tops & Shirts", "T-shirts", "Skirts", "jacket", "Dress", "Coats & Jackets", "Bottoms", "skirt");

                if (items.getCategory().equals("Tops & Shirts") || items.getCategory().equals("T-shirts") || items.getCategory().equals("Skirts")
                        || items.getCategory().equals("jacket") || items.getCategory().equals("Dress") || items.getCategory().equals("Coats & Jackets")
                        || items.getCategory().equals("Bottoms") || items.getCategory().equals("skirt")) {
                    listItemsClothes.add(items);

                    Collections.sort(listItemsClothes, new Comparator<Items>() {
                        @Override
                        public int compare(Items item1, Items item2) {
                            float price1 = (item1.getPrice() / 100) * (100 - item1.getSaleValue());
                            float price2 = (item2.getPrice() / 100) * (100 - item2.getSaleValue());
                            return Float.compare(price1, price2);
                        }
                    });
                }

                LinearLayoutManager linearLayoutManager = new GridLayoutManager(getContext(), 2);
                itemsAdapter = new ItemsAdapter(listItemsClothes, getActivity());
                itemsAdapter.notifyDataSetChanged();
                rcvListItems.setAdapter(itemsAdapter);
                rcvListItems.setLayoutManager(linearLayoutManager);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Items items = snapshot.getValue(Items.class);
//                if (listItemsSale != null){
//                    listItemsSale.add(items);
//                }
                LinearLayoutManager linearLayoutManager = new GridLayoutManager(getContext(), 2);
                itemsAdapter = new ItemsAdapter(listItemsClothes, getActivity());
                itemsAdapter.notifyDataSetChanged();
                rcvListItems.setAdapter(itemsAdapter);
                rcvListItems.setLayoutManager(linearLayoutManager);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        tvSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog();
                dialog.showBottomSheetSort(getContext(), listItemsClothes, itemsAdapter, tvSort);
            }
        });
    }

    private void initViews(View view) {
        btnSort = view.findViewById(R.id.btnSort);
        btnViewItems = view.findViewById(R.id.btnViewItems);
        tvSort = view.findViewById(R.id.tvSort);
        rcvListItems = view.findViewById(R.id.rcvListItems);
    }
}