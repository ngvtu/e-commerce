package vietmobi.net.ecommerce.activity.main;

import static vietmobi.net.ecommerce.adapter.ItemsAdapter.MY_REQUEST_CODE_DETAIL;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.activity.Dialog;
import vietmobi.net.ecommerce.adapter.CardAdapter;
import vietmobi.net.ecommerce.adapter.ItemsFavoriteAdapter;
import vietmobi.net.ecommerce.adapter.ItemsInCartAdapter;
import vietmobi.net.ecommerce.database.ItemsFavoriteDatabase;
import vietmobi.net.ecommerce.database.ItemsInCartDatabase;
import vietmobi.net.ecommerce.models.ItemsFavorite;

public class FavoriteFragment extends Fragment {

    ImageView btnSort, btnViewItems, imageView;
    TextView tvNotification, tvSort;
    RecyclerView rcvListItemFavorites;

    List<ItemsFavorite> listItemsFavorites;
    ItemsFavoriteAdapter itemsFavoriteAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);

        initViews(view);
        addEvents(view);

        return view;
    }

    private void addEvents(View view) {
        listItemsFavorites = new ArrayList<>();
        listItemsFavorites = ItemsFavoriteDatabase.getInstance(getContext()).itemsFavoriteDAO().getListItemsFavorite();
        if (listItemsFavorites.isEmpty()) {
            tvNotification.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
            return;
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        itemsFavoriteAdapter = new ItemsFavoriteAdapter(listItemsFavorites, getContext());
        itemsFavoriteAdapter.notifyDataSetChanged();
        rcvListItemFavorites.setAdapter(itemsFavoriteAdapter);
        rcvListItemFavorites.setLayoutManager(linearLayoutManager);

        tvSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog();
                dialog.showBottomSheetSortFavorite(getContext(), listItemsFavorites, itemsFavoriteAdapter, tvSort);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_REQUEST_CODE_DETAIL) {

            listItemsFavorites = ItemsFavoriteDatabase.getInstance(getContext()).itemsFavoriteDAO().getListItemsFavorite();
            itemsFavoriteAdapter.setData(listItemsFavorites);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Update list and adapter
        listItemsFavorites = ItemsFavoriteDatabase.getInstance(getContext()).itemsFavoriteDAO().getListItemsFavorite();
        if (listItemsFavorites.isEmpty()) {
            tvNotification.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
            return;
        } else {
            tvNotification.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
            ItemsFavoriteAdapter itemsFavoriteAdapter = new ItemsFavoriteAdapter(listItemsFavorites, getContext(), this);
            itemsFavoriteAdapter.setData(listItemsFavorites);
            itemsFavoriteAdapter.notifyDataSetChanged();
            updateData();
        }

    }

    public void updateData() {
        // Fetch updated data from database and update the RecyclerView
        listItemsFavorites = ItemsFavoriteDatabase.getInstance(getContext()).itemsFavoriteDAO().getListItemsFavorite();
        if (listItemsFavorites.isEmpty()) {
            tvNotification.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
            return;
        } else{
            ItemsFavoriteAdapter itemsFavoriteAdapter = new ItemsFavoriteAdapter(listItemsFavorites, getContext(), this);
            itemsFavoriteAdapter.setData(listItemsFavorites);
            itemsFavoriteAdapter.notifyDataSetChanged();
            // Update the total price text view
        }

    }

    private void initViews(View view) {
        btnSort = view.findViewById(R.id.btnSort);
        btnViewItems = view.findViewById(R.id.btnViewItems);
        tvNotification = view.findViewById(R.id.tvNotification);
        tvSort = view.findViewById(R.id.tvSort);
        rcvListItemFavorites = view.findViewById(R.id.rcvListItemFavorites);
        imageView = view.findViewById(R.id.imageView);
    }
}