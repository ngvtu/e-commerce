package vietmobi.net.ecommerce.activity.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.adapter.ItemsAdapter;
import vietmobi.net.ecommerce.models.Items;

public class ViewAllItemsNewActivity extends AppCompatActivity {

    ImageView btnBack;
    RecyclerView rcvListItemNew;

    List<Items> listItemsNew;
    ItemsAdapter itemsAdapter;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("list_items");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_items_new);

        initViews();
        addEvents();
    }

    private void addEvents() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        listItemsNew = new ArrayList<>();

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Items items = snapshot.getValue(Items.class);
                if (listItemsNew != null && items.isNew()) {
                    listItemsNew.add(items);
                }

                LinearLayoutManager linearLayoutManager = new GridLayoutManager(ViewAllItemsNewActivity.this, 2);
                itemsAdapter = new ItemsAdapter(listItemsNew,ViewAllItemsNewActivity.this);
                itemsAdapter.notifyDataSetChanged();
                rcvListItemNew.setAdapter(itemsAdapter);
                rcvListItemNew.setLayoutManager(linearLayoutManager);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Items items = snapshot.getValue(Items.class);
//                if (listItemsSale != null){
//                    listItemsSale.add(items);
//                }
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ViewAllItemsNewActivity.this, LinearLayoutManager.HORIZONTAL, false);
                itemsAdapter = new ItemsAdapter(listItemsNew, ViewAllItemsNewActivity.this);
                itemsAdapter.notifyDataSetChanged();
                rcvListItemNew.setAdapter(itemsAdapter);
                rcvListItemNew.setLayoutManager(linearLayoutManager);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void initViews() {
        btnBack = findViewById(R.id.btnBack);
        rcvListItemNew = findViewById(R.id.rcvListItemNew);
    }
}