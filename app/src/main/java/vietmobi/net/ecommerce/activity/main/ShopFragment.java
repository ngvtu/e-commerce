package vietmobi.net.ecommerce.activity.main;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.activity.oder.ViewPagerShopAdapter;
import vietmobi.net.ecommerce.adapter.ItemsAdapter;
import vietmobi.net.ecommerce.models.Items;


public class ShopFragment extends Fragment implements View.OnClickListener {
    ColorStateList def;
    ViewPager viewPager;
    ImageView btnSearch;
    SearchView searchView;
    ItemsAdapter itemsAdapter;
    List<Items> listItems;
    List<Items> searchResults;
    RecyclerView rcvSearch;
    TextView item1, item2, item3, select;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shop, container, false);

        initViews(view);
        addEvents(view);
        return view;
    }

    private void addEvents(View view) {
        item1.setOnClickListener(this);
        item2.setOnClickListener(this);
        item3.setOnClickListener(this);
        select.setOnClickListener(this);

        def = item2.getTextColors();
        btnSearch.setOnClickListener(this);


        ViewPagerShopAdapter viewPagerShopAdapter = new ViewPagerShopAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(viewPagerShopAdapter);
        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        select.animate().x(0).setDuration(100);
                        item1.setTextColor(Color.WHITE);
                        item2.setTextColor(def);
                        item3.setTextColor(def);
                        viewPager.setCurrentItem(0);
                        break;
                    case 1:
                        item1.setTextColor(def);
                        item2.setTextColor(Color.WHITE);
                        item3.setTextColor(def);
                        int size = item2.getWidth();
                        select.animate().x(size).setDuration(100);
                        viewPager.setCurrentItem(1);

                        break;
                    case 2:
                        item1.setTextColor(def);
                        item3.setTextColor(Color.WHITE);
                        item2.setTextColor(def);
                        int size2 = item2.getWidth() * 2;
                        select.animate().x(size2).setDuration(100);
                        viewPager.setCurrentItem(2);

                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private void initViews(View view) {
        item1 = view.findViewById(R.id.item1);
        item2 = view.findViewById(R.id.item2);
        item3 = view.findViewById(R.id.item3);
        select = view.findViewById(R.id.select);
        viewPager = view.findViewById(R.id.viewPager);
        searchView = view.findViewById(R.id.searchView);
        rcvSearch = view.findViewById(R.id.rcvSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
    }

    @Override
    public void onClick(View view) {
        ViewPagerShopAdapter viewPagerShopAdapter = new ViewPagerShopAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(viewPagerShopAdapter);
        switch (view.getId()) {
            case R.id.item1:
                select.animate().x(0).setDuration(100);
                item1.setTextColor(Color.WHITE);
                item2.setTextColor(def);
                item3.setTextColor(def);
                viewPager.setCurrentItem(0);

                break;
            case R.id.item2:
                item1.setTextColor(def);
                item2.setTextColor(Color.WHITE);
                item3.setTextColor(def);
                int size = item2.getWidth();
                select.animate().x(size).setDuration(100);
                viewPager.setCurrentItem(1);
                break;
            case R.id.item3:
                item1.setTextColor(def);
                item3.setTextColor(Color.WHITE);
                item2.setTextColor(def);
                int size2 = item2.getWidth() * 2;
                select.animate().x(size2).setDuration(100);
                viewPager.setCurrentItem(2);
                break;
            case R.id.btnSearch:
                Intent intent = new Intent(getContext(), SearchItemsActivity.class);
                startActivity(intent);
                break;
        }
    }
}