package vietmobi.net.ecommerce.activity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.activity.main.DetailItemActivity;
import vietmobi.net.ecommerce.activity.main.FavoriteFragment;
import vietmobi.net.ecommerce.adapter.ItemsAdapter;
import vietmobi.net.ecommerce.adapter.ItemsFavoriteAdapter;
import vietmobi.net.ecommerce.database.ItemsFavoriteDatabase;
import vietmobi.net.ecommerce.models.Items;
import vietmobi.net.ecommerce.models.ItemsFavorite;

public class Dialog implements View.OnClickListener {
//    Context context;

    public void showDialogSelectSize(Context  context, Items item) {
        android.app.Dialog dialog = new android.app.Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bottom_sheet_select_size);
//        createDialog();

        TextView layout_size_info = dialog.findViewById(R.id.layout_size_info);
        AppCompatAutoCompleteTextView edtSize = dialog.findViewById(R.id.edtSize);
        AppCompatAutoCompleteTextView edtColor = dialog.findViewById(R.id.edtColor);

        TextInputLayout layout_size = dialog.findViewById(R.id.layout_size), layout_color = dialog.findViewById(R.id.layout_color);

        String[] size = {"XS", "S", "M", "L", "XL"};
        String[] color = {"Back", "Gray", "White"};

        ArrayAdapter<String> adapterSize;
        adapterSize = new ArrayAdapter<String>(context, R.layout.line_list_type_items, size);
        edtSize.setAdapter(adapterSize);
        edtSize.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String size = parent.getItemAtPosition(position).toString();
                edtSize.setText(size);
            }
        });

        ArrayAdapter<String> adapterColor;
        adapterColor = new ArrayAdapter<String>(context, R.layout.line_list_type_items, color);
        edtColor.setAdapter(adapterColor);
        edtColor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String color = parent.getItemAtPosition(position).toString();
                edtColor.setText(color);
            }
        });
        TextView btnAddFavorite = dialog.findViewById(R.id.btnAddFavorite);

        btnAddFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemsFavoriteAdapter itemsFavoriteAdapter = new ItemsFavoriteAdapter();

                if (!edtColor.getText().toString().equals("") && !edtSize.getText().toString().equals("")) {
                    layout_size.setError("");
                    layout_color.setError("");
                    String size = edtSize.getText().toString();
                    String color = edtColor.getText().toString();
                    float price = 0;
                    if (item.isSale()) {
//                price = ;
                        price = (float) (Math.round((item.getPrice() / 100) * (100 - item.getSaleValue()) * 100.0) / 100.0);
                    } else if (item.isNew()) {
                        price = item.getPrice();
                    }
                    ItemsFavorite itemsFavorite = new ItemsFavorite(item.getId(), item.getImgItems(), item.getItemName(), item.getOfBrand(),
                            item.getCategory(), size, color, price, 1, item.isSale(), true, item.isNew(), false);

                    ItemsFavoriteDatabase.getInstance(context).itemsFavoriteDAO().insertItemsFavorite(itemsFavorite);

//                    Toast.makeText(this, "Add to favorite", Toast.LENGTH_SHORT).show();
                    List<ItemsFavorite> listItemsFavorites;
                    listItemsFavorites = ItemsFavoriteDatabase.getInstance(context).itemsFavoriteDAO().getListItemsFavorite();

                    if (listItemsFavorites == null) {
                        return;
                    }
                    FavoriteFragment favoriteFragment = new FavoriteFragment();

                    favoriteFragment.updateData();
                    itemsFavoriteAdapter.setData(listItemsFavorites);
                    itemsFavoriteAdapter.notifyDataSetChanged();
                    dialog.dismiss();

                } else if (edtSize.getText().toString().equals("")) {
                    edtSize.requestFocus();
                    layout_size.setError("*Choose size");
                    layout_color.setError("");
                } else if (edtColor.getText().toString().equals("")) {
                    edtColor.requestFocus();
                    layout_color.setError("*Choose color");
                    layout_size.setError("");
                }
            }
        });

        layout_size_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = new Toast(context);
                toast.setDuration(Toast.LENGTH_SHORT);

                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.toast_size_info, null);
                toast.setView(view);

                ImageView image = view.findViewById(R.id.toast_image);
                image.setImageResource(R.drawable.info_size);
                toast.show();
            }
        });

        dialog.show();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setGravity(Gravity.BOTTOM);
    }

    public void showBottomSheetSort(Context context, List<Items> listItemsNewAndSale, ItemsAdapter itemsAdapter, TextView tvSort){
        android.app.Dialog dialog = new android.app.Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bottom_sheet_sort);

        TextView tvSortLowToHigh = dialog.findViewById(R.id.tvSortLowToHigh);
        TextView tvSortHighToLow = dialog.findViewById(R.id.tvSortHighToLow);
        TextView tvSortNewest = dialog.findViewById(R.id.tvSortNewest);

        tvSortHighToLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Sort High To Low", Toast.LENGTH_SHORT).show();
                Collections.sort(listItemsNewAndSale, new Comparator<Items>() {
                    @Override
                    public int compare(Items item1, Items item2) {
                        float price1 = (item1.getPrice() / 100) * (100 - item1.getSaleValue());
                        float price2 = (item2.getPrice() / 100) * (100 - item2.getSaleValue());
                        return Float.compare(price2, price1);

                    }
                });
                itemsAdapter.notifyDataSetChanged();
                tvSort.setText("Sort: high to low");
                dialog.dismiss();
            }
        });

        tvSortLowToHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Sort Low To High", Toast.LENGTH_SHORT).show();
                Collections.sort(listItemsNewAndSale, new Comparator<Items>() {
                    @Override
                    public int compare(Items item1, Items item2) {
                        float price1 = (item1.getPrice() / 100) * (100 - item1.getSaleValue());
                        float price2 = (item2.getPrice() / 100) * (100 - item2.getSaleValue());
                        return Float.compare(price1, price2);
                    }
                });

                // Notify the adapter that the list has been updated
                itemsAdapter.notifyDataSetChanged();
                tvSort.setText("Sort: low to high");
                dialog.dismiss();

            }
        });

        tvSortNewest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Sort list by newest
                Collections.sort(listItemsNewAndSale, new Comparator<Items>() {
                    @Override
                    public int compare(Items item1, Items item2) {
                        if (item1.isNew() && !item2.isNew()) {
                            return -1;
                        } else if (!item1.isNew() && item2.isNew()) {
                            return 1;
                        } else {
                            return 0;
                        }
                    }
                });

                // Notify the adapter that the list has been updated
                itemsAdapter.notifyDataSetChanged();
                tvSort.setText("Newest");
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setGravity(Gravity.BOTTOM);
    }
    public void showBottomSheetSortFavorite(Context context, List<ItemsFavorite> listItemsNewAndSale, ItemsFavoriteAdapter itemsAdapter, TextView tvSort){
        android.app.Dialog dialog = new android.app.Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bottom_sheet_sort);

        TextView tvSortLowToHigh = dialog.findViewById(R.id.tvSortLowToHigh);
        TextView tvSortHighToLow = dialog.findViewById(R.id.tvSortHighToLow);
        TextView tvSortNewest = dialog.findViewById(R.id.tvSortNewest);

        tvSortHighToLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Sort High To Low", Toast.LENGTH_SHORT).show();
                Collections.sort(listItemsNewAndSale, new Comparator<ItemsFavorite>() {
                    @Override
                    public int compare(ItemsFavorite item1, ItemsFavorite item2) {
                        float price1 = item1.getPrice();
                        float price2 = item2.getPrice();
                        return Float.compare(price2, price1);
                    }
                });
                itemsAdapter.notifyDataSetChanged();
                tvSort.setText("Sort: high to low");
                dialog.dismiss();
            }
        });

        tvSortLowToHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Sort Low To High", Toast.LENGTH_SHORT).show();
                Collections.sort(listItemsNewAndSale, new Comparator<ItemsFavorite>() {
                    @Override
                    public int compare(ItemsFavorite item1, ItemsFavorite item2) {
                        float price1 = item1.getPrice();
                        float price2 = item2.getPrice();
                        return Float.compare(price1, price2);
                    }
                });
                itemsAdapter.notifyDataSetChanged();
                tvSort.setText("Sort: low to high");
                dialog.dismiss();

            }
        });

        tvSortNewest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Sort list by newest
                Collections.sort(listItemsNewAndSale, new Comparator<ItemsFavorite>() {
                    @Override
                    public int compare(ItemsFavorite item1, ItemsFavorite item2) {
                        if (item1.isNew() && !item2.isNew()) {
                            return -1;
                        } else if (!item1.isNew() && item2.isNew()) {
                            return 1;
                        } else {
                            return 0;
                        }

                    }
                });

                // Notify the adapter that the list has been updated
                itemsAdapter.notifyDataSetChanged();
                tvSort.setText("Newest");
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setGravity(Gravity.BOTTOM);
    }




    @Override
    public void onClick(View v) {

    }
}
