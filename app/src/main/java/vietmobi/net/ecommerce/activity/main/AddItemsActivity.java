package vietmobi.net.ecommerce.activity.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.AppCompatCheckBox;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.activity.AddPromoCodeActivity;
import vietmobi.net.ecommerce.models.Items;

public class AddItemsActivity extends AppCompatActivity {
    ImageView btnBack, imvAddImage;
    TextInputLayout layout_id_item, layout_name_item, layout_sale, layout_of_brand, layout_category, layout_price, layout_description;
    TextInputEditText edtIdItem, edtNameItem, edtSaleValue, edtPrice, edtDescribe;
    AppCompatAutoCompleteTextView edtOfBrand, edtCategory;
    TextView btnSaveItem, btnUpdateItem;
    AppCompatCheckBox cbxIsSale, cbxIsNew;

    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_items);

        initViews();
        addEvents();
    }

    private void addEvents() {
        String[] itemsOfBrand = {"13 MONTH", "BOO LAAB", "BOOZ ILLA", "BUCK MASON", "FE NOEL", "IRO", "KYE", "MAJE", "OOB"};
        String[] itemsOfCategory = {"Skirts", "Tops & Shirts", "Accessory", "Bags", "Boots", "Bottoms", "Cardigan", "Coats & Jackets", "Dress", "Jacket", "Jeans", "T-shirts"};

        ArrayAdapter<String> adapterItemsOfBrand;
        adapterItemsOfBrand = new ArrayAdapter<String>(AddItemsActivity.this, R.layout.line_list_type_items, itemsOfBrand);
        edtOfBrand.setAdapter(adapterItemsOfBrand);

        ArrayAdapter<String> adapterItemsOfCategory;
        adapterItemsOfCategory = new ArrayAdapter<String>(AddItemsActivity.this, R.layout.line_list_type_items, itemsOfCategory);
        edtCategory.setAdapter(adapterItemsOfCategory);

        edtOfBrand.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                edtOfBrand.setText(item);
            }
        });
        edtCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                edtCategory.setText(item);
            }
        });

        cbxIsSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbxIsSale.isChecked()) {
                    layout_sale.setVisibility(View.VISIBLE);
                } else {
                    layout_sale.setVisibility(View.GONE);
                }
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imvAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent();
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, 112);
            }
        });


        btnSaveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idItem = edtIdItem.getText().toString().trim();
                String nameItem = edtNameItem.getText().toString().trim();
                String describe = edtDescribe.getText().toString().trim();
                String ofBrand = edtOfBrand.getText().toString().trim();
                String category = edtCategory.getText().toString().trim();
                Items items = new Items();
                float price, saleValue;
                boolean isSale = false, isNew = false;
                try {
                    price = Float.parseFloat(edtPrice.getText().toString().trim());
                    saleValue = Float.parseFloat(edtSaleValue.getText().toString().trim());
                    if (saleValue != 0) {
                        isSale = true;
                    }
                } catch (Exception e) {
                    return;
                }
                if (cbxIsNew.isChecked()) {
                    isNew = true;
                }

                items = new Items(idItem, nameItem, ofBrand, category, describe, price, saleValue, isSale, isNew);

                FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                DatabaseReference myRef = firebaseDatabase.getReference("list_items");

                myRef.child(idItem).setValue(items, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                        Toast.makeText(AddItemsActivity.this, "Add items complete!", Toast.LENGTH_SHORT).show();
                        edtIdItem.setText("");
                        edtNameItem.setText("");
                        edtDescribe.setText("");
                        edtOfBrand.setText("");
                        edtCategory.setText("");
                        edtPrice.setText("");
                        edtSaleValue.setText("");
                        cbxIsSale.setChecked(false);
                        cbxIsNew.setChecked(false);
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 112 && resultCode == RESULT_OK && data != null) {
            imageUri = data.getData();
//            imvA
        }
    }

    private void initViews() {
        btnBack = findViewById(R.id.btnBack);
        layout_id_item = findViewById(R.id.layout_id_item);
        layout_name_item = findViewById(R.id.layout_name_item);
        layout_of_brand = findViewById(R.id.layout_of_brand);
        layout_category = findViewById(R.id.layout_category);
        layout_price = findViewById(R.id.layout_price);
        layout_sale = findViewById(R.id.layout_sale);
        layout_description = findViewById(R.id.layout_description);
        edtIdItem = findViewById(R.id.edtIdItem);
        edtNameItem = findViewById(R.id.edtNameItem);
        edtIdItem = findViewById(R.id.edtIdItem);
        edtOfBrand = findViewById(R.id.edtOfBrand);
        edtCategory = findViewById(R.id.edtCategory);
        edtPrice = findViewById(R.id.edtPrice);
        edtSaleValue = findViewById(R.id.edtSaleValue);
        edtDescribe = findViewById(R.id.edtDescribe);
        btnSaveItem = findViewById(R.id.btnSaveItem);
        btnUpdateItem = findViewById(R.id.btnUpdateItem);
        cbxIsSale = findViewById(R.id.cbxIsSale);
        cbxIsNew = findViewById(R.id.cbxIsNew);
        imvAddImage = findViewById(R.id.imvAddImage);

    }
}