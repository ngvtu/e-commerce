package vietmobi.net.ecommerce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.activity.main.BagFragment;
import vietmobi.net.ecommerce.activity.main.FavoriteFragment;
import vietmobi.net.ecommerce.database.ItemsFavoriteDatabase;
import vietmobi.net.ecommerce.database.ItemsInCartDatabase;
import vietmobi.net.ecommerce.models.ItemsFavorite;
import vietmobi.net.ecommerce.models.ItemsInCart;

public class ItemsFavoriteAdapter extends RecyclerView.Adapter<ItemsFavoriteAdapter.ViewHolder> {

    private List<ItemsFavorite> listItemsFavorites;
    Context context;
    ItemsInCart itemsInCart;
    FavoriteFragment favoriteFragment;

    public ItemsFavoriteAdapter() {
    }

    public ItemsFavoriteAdapter(List<ItemsFavorite> listItemsFavorites, Context context, FavoriteFragment favoriteFragment) {
        this.listItemsFavorites = listItemsFavorites;
        this.context = context;
        this.favoriteFragment = favoriteFragment;
    }

    public void setData(List<ItemsFavorite> listItemsFavorites) {
        this.listItemsFavorites = listItemsFavorites;
        notifyDataSetChanged();
    }


    public ItemsFavoriteAdapter(List<ItemsFavorite> listItemsFavorites, Context context) {
        this.listItemsFavorites = listItemsFavorites;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.line_item_list_in_favorite, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemsFavorite itemsFavorite = listItemsFavorites.get(position);

        holder.tvNameItem.setText(itemsFavorite.getItemName());
        holder.tvNameBrand.setText(itemsFavorite.getOfBrand());
        holder.tvSize.setText(itemsFavorite.getSize());
        holder.tvColor.setText(itemsFavorite.getColor());
        holder.tvPrice.setText(Float.toString(itemsFavorite.getPrice()));
        String urlImage = itemsFavorite.getImgItems();
        ImageView imageView = holder.itemView.findViewById(R.id.imgItem);
        Glide.with(context).load(urlImage).into(imageView);

        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ItemsFavoriteDatabase.getInstance(view.getContext()).itemsFavoriteDAO().deleteItemsFavorite(itemsFavorite);
                listItemsFavorites = ItemsFavoriteDatabase.getInstance(context).itemsFavoriteDAO().getListItemsFavorite();
                setData(listItemsFavorites);
                notifyDataSetChanged();
            }
        });


        holder.btnAddToBag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemsInCart = new ItemsInCart(itemsFavorite.getIdItem(), itemsFavorite.getImgItems(), itemsFavorite.getItemName(),
                        itemsFavorite.getOfBrand(), itemsFavorite.getCategory(), itemsFavorite.getSize(), itemsFavorite.getColor(), itemsFavorite.getPrice(), itemsFavorite.getCount(), true);

                ItemsInCartDatabase.getInstance(v.getContext()).itemsInCartDAO().insertItemInCart(itemsInCart);
                List<ItemsInCart> listItemsInCarts;
                listItemsInCarts = ItemsInCartDatabase.getInstance(context).itemsInCartDAO().getListItemInCart();
                if (listItemsInCarts == null) {
                    return;
                }
                ItemsInCartAdapter itemsInCartAdapter = new ItemsInCartAdapter(listItemsInCarts, context);
                itemsInCartAdapter.setData(listItemsInCarts);
//                BagFragment bagFragment = new BagFragment();
//                bagFragment.updateData();

                ItemsFavoriteDatabase.getInstance(v.getContext()).itemsFavoriteDAO().deleteItemsFavorite(itemsFavorite);
                listItemsFavorites = ItemsFavoriteDatabase.getInstance(context).itemsFavoriteDAO().getListItemsFavorite();
                setData(listItemsFavorites);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItemsFavorites.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgItem, btnRemove;
        ImageButton btnAddToBag;
        TextView tvNameItem, tvNameBrand, tvColor, tvSize, tvIsNew, tvIsSale, tvPrice;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.imgItem = itemView.findViewById(R.id.imgItem);
            this.btnRemove = itemView.findViewById(R.id.btnRemove);
            this.btnAddToBag = itemView.findViewById(R.id.btnAddToBag);
            this.tvNameItem = itemView.findViewById(R.id.tvNameItem);
            this.tvNameBrand = itemView.findViewById(R.id.tvNameBrand);
            this.tvColor = itemView.findViewById(R.id.tvColor);
            this.tvSize = itemView.findViewById(R.id.tvSize);
            this.tvIsNew = itemView.findViewById(R.id.tvIsNew);
            this.tvIsSale = itemView.findViewById(R.id.tvIsSale);
            this.tvPrice = itemView.findViewById(R.id.tvPrice);
        }
    }
}
