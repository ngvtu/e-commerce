package vietmobi.net.ecommerce.adapter;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import vietmobi.net.ecommerce.OnApplyButtonClickListener;
import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.models.PromoCode;


public class PromoCodeAdapter extends RecyclerView.Adapter<PromoCodeAdapter.ViewHolder> {

    List<PromoCode> listPromoCode;
    Context context;

    private OnApplyButtonClickListener onApplyButtonClickListener;


    public void setOnApplyButtonClickListener(OnApplyButtonClickListener onApplyButtonClickListener) {
        this.onApplyButtonClickListener = onApplyButtonClickListener;
    }

    public PromoCodeAdapter(List<PromoCode> listPromoCode, Context context) {
        this.listPromoCode = listPromoCode;
        this.context = context;
    }

    public void setData(List<PromoCode> listPromoCode) {
        this.listPromoCode = listPromoCode;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.line_promo_code_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PromoCode promoCode = listPromoCode.get(position);
        if (promoCode == null) {
            return;
        }

        holder.tvNameCode.setText(promoCode.getCodeName());
        holder.tvValuePromo.setText(String.valueOf(promoCode.getValuePromo()));
        holder.tvCode.setText(promoCode.getCode());
        holder.tvDayRemain.setText(String.valueOf(promoCode.getRemainingDay()));

        holder.btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("promoCode", MODE_PRIVATE);

                SharedPreferences.Editor myEdit = sharedPreferences.edit();

                myEdit.putString("code", promoCode.getCode());
                myEdit.putInt("valuePromo", promoCode.getValuePromo());

                myEdit.apply();

                Toast.makeText(context, "Tesst", Toast.LENGTH_SHORT).show();
                ((Activity)context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listPromoCode.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvValuePromo, tvNameCode, tvCode, tvDayRemain, btnApply;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvValuePromo = itemView.findViewById(R.id.tvValuePromo);
            tvNameCode = itemView.findViewById(R.id.tvNameCode);
            tvCode = itemView.findViewById(R.id.tvCode);
            tvDayRemain = itemView.findViewById(R.id.tvDayRemain);
            btnApply = itemView.findViewById(R.id.btnApply);
        }
    }
}
