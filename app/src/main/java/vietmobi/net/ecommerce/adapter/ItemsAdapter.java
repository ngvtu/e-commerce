package vietmobi.net.ecommerce.adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.activity.Dialog;
import vietmobi.net.ecommerce.activity.main.DetailItemActivity;
import vietmobi.net.ecommerce.models.Items;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> {

    public static final int MY_REQUEST_CODE_DETAIL = 678;
    private List<Items> listItems;
    private List<Items> mListItems;
    Context context;

    public void setData(List<Items> listItems){
        this.listItems = listItems;
        notifyDataSetChanged();
    }

    public ItemsAdapter(List<Items> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.line_item_new_list_in_home, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Items items = listItems.get(position);
        Glide.with(context).load(items.getImgItems()).into(holder.imvItem);

        if (items.isSale()) {
            holder.tvIsSale.setVisibility(View.VISIBLE);
            holder.tvIsSale.setText("-" + items.getSaleValue().toString() + "%");
            holder.layout_new_price.setVisibility(View.VISIBLE);
            holder.tvPrice.setText(items.getPrice().toString());
            holder.tvNameItem.setText(items.getItemName());
            float price = (items.getPrice() / 100) * (100 - items.getSaleValue());
            float newPrice = (float) (Math.round(price * 100.0) / 100.0);
            holder.tvNewPrice.setText(Float.toString(newPrice));
            holder.tvPrice.setTextColor(Color.parseColor("#9B9B9B"));
            holder.textView2.setTextColor(Color.parseColor("#9B9B9B"));
            holder.imageView8.setVisibility(View.VISIBLE);
        } else if (items.isNew()) {
            holder.tvIsNew.setVisibility(View.VISIBLE);
            holder.tvIsSale.setVisibility(View.GONE);
            holder.layout_new_price.setVisibility(View.GONE);
            holder.imageView8.setVisibility(View.GONE);
            holder.tvNameItem.setText(items.getItemName());
            holder.tvBrandName.setText(items.getOfBrand());
            holder.tvPrice.setText(String.valueOf(items.getPrice()));

        } else{
            holder.tvIsNew.setVisibility(View.GONE);
            holder.tvIsSale.setVisibility(View.GONE);
            holder.layout_new_price.setVisibility(View.GONE);
            holder.imageView8.setVisibility(View.GONE);
            holder.tvNameItem.setText(items.getItemName());
            holder.tvBrandName.setText(items.getOfBrand());
            holder.tvPrice.setText(String.valueOf(items.getPrice()));

        }

        holder.btnAddFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog();
                dialog.showDialogSelectSize(context, items);
                Toast.makeText(context, "Add Favorite", Toast.LENGTH_SHORT).show();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailItemActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("item", items);
                intent.putExtras(bundle);
                ((Activity) context).startActivityForResult(intent, MY_REQUEST_CODE_DETAIL);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) {

                mListItems = (ArrayList<Items>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<Items> FilteredArrList = new ArrayList<Items>();

                if (listItems == null) {
                    listItems = new ArrayList<Items>(mListItems); // saves the original data in listItems
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the listItems(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = listItems.size();
                    results.values = listItems;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < listItems.size(); i++) {
                        String data = listItems.get(i).getItemName();
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            FilteredArrList.add(listItems.get(i));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvItem, btnAddFavorite, imageView8;
        TextView tvBrandName, tvNameItem, tvPrice, tvIsNew, tvSaleValue, tvNewPrice, tvIsSale, textView2;
        LinearLayout layout_new_price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.imvItem = itemView.findViewById(R.id.imgItem);
            this.btnAddFavorite = itemView.findViewById(R.id.btnAddFavorite);
            this.tvBrandName = itemView.findViewById(R.id.tvBrandName);
            this.tvNameItem = itemView.findViewById(R.id.tvNameItem);
            this.tvPrice = itemView.findViewById(R.id.tvPrice);
            this.tvIsNew = itemView.findViewById(R.id.tvIsNew);
            this.tvSaleValue = itemView.findViewById(R.id.tvPriceOld);
            this.tvNewPrice = itemView.findViewById(R.id.tvPriceNew);
            this.tvIsSale = itemView.findViewById(R.id.tvIsSale);
            this.layout_new_price = itemView.findViewById(R.id.layout_new_price);
            this.imageView8 = itemView.findViewById(R.id.imageView8);
            this.textView2 = itemView.findViewById(R.id.textView2);
        }
    }
}
