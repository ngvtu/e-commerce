package vietmobi.net.ecommerce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;

import java.util.List;

import vietmobi.net.ecommerce.R;
import vietmobi.net.ecommerce.activity.main.BagFragment;
import vietmobi.net.ecommerce.database.ItemsInCartDatabase;
import vietmobi.net.ecommerce.models.ItemsInCart;

public class ItemsInCartAdapter extends RecyclerView.Adapter<ItemsInCartAdapter.ViewHolder> {

    List<ItemsInCart> listItemInCart;
    Context context;
    BagFragment bagFragment;

    public ItemsInCartAdapter(List<ItemsInCart> listItemInCart, Context context) {
        this.listItemInCart = listItemInCart;
        this.context = context;
    }

    public ItemsInCartAdapter(List<ItemsInCart> listItemInCart, Context context, BagFragment bagFragment) {
        this.listItemInCart = listItemInCart;
        this.context = context;
        this.bagFragment = bagFragment;
    }
//
//    public ItemsInCartAdapter(List<ItemsInCart> listItemInCart, Context context, BagFragment bagFragment) {
//        this.listItemInCart = listItemInCart;
//        this.context = context;
//        this.bagFragment = bagFragment;
//    }


    public void setData(List<ItemsInCart> listItemInCart) {
        this.listItemInCart = listItemInCart;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.line_item_list_in_bag, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemsInCart itemsInCart = listItemInCart.get(position);

        holder.tvNameItem.setText(itemsInCart.getItemName());
        holder.tvSize.setText(itemsInCart.getSize());
        holder.tvColor.setText(itemsInCart.getColor());
        holder.tvPrice.setText(String.valueOf(itemsInCart.getPrice()));
        holder.tvCount.setText(String.valueOf(itemsInCart.getCount()));

        Glide.with(context).load(itemsInCart.getImgItem()).into(holder.imgItem);

        holder.btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Menu", Toast.LENGTH_SHORT).show();
            }
        });

        holder.btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = itemsInCart.getCount();
                float price = itemsInCart.getPrice() / count;
                float countPrice = 0;

                if (count > 1) {
                    count--;
                    countPrice = price * count;
                    countPrice = (float) (Math.round(countPrice * 100.0) / 100.0);

                    itemsInCart.setPrice(countPrice);
                    holder.tvPrice.setText(String.valueOf(countPrice));
                    itemsInCart.setCount(count);
                    holder.tvCount.setText(String.valueOf(count));

                    ItemsInCartDatabase.getInstance(context).itemsInCartDAO().updateItemsInCart(itemsInCart);
                    listItemInCart = ItemsInCartDatabase.getInstance(context).itemsInCartDAO().getListItemInCart();
                    setData(listItemInCart);
                    bagFragment.updateData();

                } else {
                    ItemsInCartDatabase.getInstance(context).itemsInCartDAO().deleteItemsInCart(itemsInCart);
                    listItemInCart = ItemsInCartDatabase.getInstance(context).itemsInCartDAO().getListItemInCart();
                    setData(listItemInCart);
                    notifyDataSetChanged();
                    Toast.makeText(context, "Delete items", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = itemsInCart.getCount();
                count++;
                float price = itemsInCart.getPrice();
                float countPrice = 0;
                countPrice = price * count;
                countPrice = (float) (Math.round(countPrice * 100.0) / 100.0);

                itemsInCart.setPrice(countPrice);
                holder.tvPrice.setText(String.valueOf(countPrice));

                itemsInCart.setCount(count);
                holder.tvCount.setText(String.valueOf(count));

                ItemsInCartDatabase.getInstance(context).itemsInCartDAO().updateItemsInCart(itemsInCart);
                listItemInCart = ItemsInCartDatabase.getInstance(context).itemsInCartDAO().getListItemInCart();
                setData(listItemInCart);

                bagFragment.updateData();
            }
        });

        holder.btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(v.getContext(), v);

                popupMenu.inflate(R.menu.menu_items_in_cart);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_delete:
                                ItemsInCartDatabase.getInstance(context).itemsInCartDAO().deleteItemsInCart(itemsInCart);
                                listItemInCart = ItemsInCartDatabase.getInstance(context).itemsInCartDAO().getListItemInCart();
                                setData(listItemInCart);
                                bagFragment.updateData();
                                notifyDataSetChanged();
                                Toast.makeText(context, "Delete items", Toast.LENGTH_SHORT).show();
                                return true;
                            case R.id.menu_favorite:
                                Toast.makeText(context, "Add to Favorite Coming soon", Toast.LENGTH_SHORT).show();
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popupMenu.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return listItemInCart.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgItem, btnMenu;
        ImageButton btnSub, btnPlus;
        TextView tvColor, tvSize, tvNameItem, tvPrice, tvCount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.btnMenu = itemView.findViewById(R.id.btnMenu);
            this.imgItem = itemView.findViewById(R.id.imgItem);
            this.btnSub = itemView.findViewById(R.id.btnSub);
            this.btnPlus = itemView.findViewById(R.id.btnPlus);
            this.tvColor = itemView.findViewById(R.id.tvColor);
            this.tvSize = itemView.findViewById(R.id.tvSize);
            this.tvNameItem = itemView.findViewById(R.id.tvNameItem);
            this.tvPrice = itemView.findViewById(R.id.tvPrice);
            this.tvCount = itemView.findViewById(R.id.tvCount);

        }
    }
}