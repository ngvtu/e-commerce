package vietmobi.net.ecommerce;

public interface OnApplyButtonClickListener {
    void onApplyButtonClick();
}
